from linebot.models import (
    MessageEvent, TextMessage, TextSendMessage,
    TemplateSendMessage, CarouselTemplate, PostbackEvent,
    StickerMessage, StickerSendMessage, ImagemapSendMessage,
    ImageMessage, ImageSendMessage, QuickReply, QuickReplyButton,
    LocationAction
)

def postback_process(userid, db, event, line_bot_api):
    ret = []
    print(db.get_all_users())

    # choose the event first
    if "point=" in event.postback.data:
        print("我戳了戳了")
        # record user's eventid
        point = float(event.postback.data.replace("point=", ""))
        db.add_user_score(userid, point)
        cur_qid = db.get_cur_qid(userid)
        db.set_cur_qid(userid, cur_qid + 1)

    return ret
