from linebot.models import (
    FlexSendMessage
)

def ques_flex(idx, ques, opt_list, point_list):
    flex_content = {
        "type": "bubble",
        "body": {
            "type": "box",
            "layout": "vertical",
            "contents": [
                {
                    "type": "text",
                    "text": f"Q{idx}",
                    "weight": "bold",
                    "size": "xxl",
                    "gravity": "center",
                    "wrap": True
                },
                {
                    "type": "text",
                    "text": f"{ques}",
                    "weight": "bold",
                    "size": "xxl",
                    "wrap": True,
                    "margin": "xs",
                    "offsetStart": "sm"
                }
            ]
        },
        "footer": {
            "type": "box",
            "layout": "vertical",
            "spacing": "sm",
            "contents": [], # Options
            "flex": 0
        }
    }

    for opt, point in zip(opt_list, point_list):
        button = {
            "type": "button",
            "style": "link",
            "height": "sm",
            "action": {
                "type": "postback",
                "label": f"{opt}",
                "data": f"point={point}",
                "text": f"您說：{opt}",
            }
        }
        flex_content["footer"]["contents"].append(button)

    flex_message = FlexSendMessage(
        alt_text="事件選單", contents=flex_content)
    return flex_message

def fund_flex(suggest_list):
    flex_content = {
        "type": "bubble",
        "size": "giga",
        "body": {
            "type": "box",
            "layout": "vertical",
            "contents": [
                {
                    "type": "text",
                    "text": "我們為你推薦的基金有：",
                    "weight": "bold",
                    "size": "xl",
                    "wrap": True
                }
            ]
        },
        "footer": {
            "type": "box",
            "layout": "vertical",
            "spacing": "sm",
            "contents": [], # fund suggestion
            "flex": 0
        }
    }

    for afund in suggest_list:
        button = {
            "type": "button",
            "style": "link",
            "height": "sm",
            "action": {
                "type": "uri",
                "label": f"{afund['name']}",
                "uri": f"{afund['ref']}",
            }
        }
        flex_content["footer"]["contents"].append(button)

    flex_message = FlexSendMessage(
        alt_text="事件選單", contents=flex_content)
    return flex_message
