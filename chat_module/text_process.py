from linebot.models import (
    TextSendMessage, TemplateSendMessage, CarouselTemplate, StickerSendMessage,
)

from tool.flex import fund_flex
from tool.risk_eval import read_ques
from tool.fund_list import choose_fund

def text_process(text, userid, line_bot_api, db):
    print(f"This is user: {userid}")

    ret = []
    if userid not in db.get_all_users():
        db.add_user(userid)

    if db.get_is_questioning(userid):
        if "您說：" in text:
            question_table = read_ques()

            if db.get_cur_qid(userid) == len(question_table): # finish questionnaire
                # 1. Save result
                db.set_user_risk_result(userid, db.get_user_score(userid))
                # 2. Reset temp
                db.reset_user(userid)
                # 3. Choose fund
                fund_list = choose_fund(db.get_user_score(userid))
                db.set_user_myfund(userid, fund_list)
                ret = fund_flex(fund_list)
            else:
                ret = question_table[db.get_cur_qid(userid)]
        else:
            ret = TextSendMessage(text=f"請點選選項唷！")
    else:
        if text == "我現在是什麼基金":
            if db.get_user_myfund(userid) == []:
                ret = TextSendMessage(text=f"請先填寫問卷唷！")
            else:
                fund_list = choose_fund(db.get_user_score(userid), num_suggestion = 5)
                ret = fund_flex(fund_list)
        if text == "我要來做風險評估":
            db.set_is_questioning(userid, True)
            db.set_cur_qid(userid, 0)
            question_table = read_ques()
            ret = question_table[db.get_cur_qid(userid)]

    return ret
