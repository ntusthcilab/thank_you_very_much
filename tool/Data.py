import pickle

class Data:
    def __init__(self):
        self.data = {}
        self.filename = "./src/data.pkl"
        with open(self.filename, 'wb') as fh:
            pickle.dump(self.data, fh)

    def read(self):
        with open(self.filename, 'rb') as fh:
            self.data = pickle.load(fh)
        return

    def write(self):
        with open(self.filename, 'wb') as fh:
            pickle.dump(self.data, fh)
        return

    # Getter
    def get_all_users(self):
        self.read()
        return self.data.keys()

    def get_cur_qid(self, userid):
        self.read()
        return self.data[userid]["cur_qid"]

    def get_is_questioning(self, userid):
        self.read()
        return self.data[userid]["is_questioning"]

    def get_user_score(self, userid):
        self.read()
        return self.data[userid]["score"]

    def get_user_myfund(self, userid):
        self.read()
        return self.data[userid]["my_portfolio"]

    # Setter
    def add_user(self, userid):
        self.read()
        if userid not in self.data:
            self.data[userid] = {
                "is_questioning": False,
                "cur_qid": -1,
                "score": -1,        # temp score in filling the naire (-1: 沒填過, -2: 已填完)
                "risk_result": -1,  # result
                "my_portfolio": [],
            }
            self.write()
        else:
            print(f"User {userid} has already add the Bot!")

    def set_is_questioning(self, userid, res):
        self.read()
        self.data[userid]["is_questioning"] = res
        self.write()
        return

    def set_cur_qid(self, userid, qid):
        self.read()
        self.data[userid]["cur_qid"] = qid
        self.write()
        return

    def add_user_score(self, userid, point):
        self.read()
        if self.data[userid]["score"] == -1:
            self.data[userid]["score"] = point
        else:
            self.data[userid]["score"] += point
        self.write()
        return

    def set_user_score(self, userid, score):
        self.read()
        self.data[userid]["score"] = score
        self.write()
        return

    def set_user_risk_result(self, userid, score):
        self.read()
        self.data[userid]["risk_result"] = score
        self.write()
        return

    def set_user_myfund(self, userid, my_fund):
        self.read()
        self.data[userid]["my_portfolio"] = my_fund
        self.write()
        return

    # Utils
    def reset_user(self, userid):
        self.read()
        self.set_user_score(userid, -2) # HAD finished
        self.set_cur_qid(userid, -1)
        self.set_is_questioning(userid, False)
        self.write()
        return
