import csv
from tool.flex import ques_flex

def read_ques():
    table = []
    with open('./src/risk_eval.csv', encoding = "utf-8-sig") as csvfile:
        all_rows = list(csv.reader(csvfile))
        sum_row_wise = 0
        row_weight = []

        for i in range(0, len(all_rows), 2):
            # Column-wise
            ques = list(filter(None, all_rows[i]))
            opts = list(filter(None, all_rows[i + 1]))
            opts = [float(opt) for opt in opts]
            col_points = opts[1:] # options points
            col_cnt = (max(col_points) - min(col_points)) if (max(col_points) - min(col_points)) > 1 else 1

            flex = {
                "ques": ques[0],
                "options": ques[1:],
                "num_options": len(ques[1:]),
                "points": [((opt - min(col_points)) / col_cnt) for opt in col_points]
            }
            table.append(flex)

            # Row-wise
            sum_row_wise += opts[0]
            row_weight.append(opts[0])

        # Normalize the points to 100
        for i in range(len(table)):
            for j in range(len(table[i]["points"])):
                opt_point = row_weight[i] * 100 / sum_row_wise
                table[i]["points"][j] = round(table[i]["points"][j] * opt_point, 4)

    flex_table = []
    for qid in range(len(table)):
        flex_table.append(ques_flex(qid + 1, table[qid]["ques"], table[qid]["options"], table[qid]["points"]))

    return flex_table # List of FlexSendMessage
