import csv, random

def fund_pool():
    fund_pool = [] # from risk averse to risk lover
    with open('./src/fund_list.csv', encoding = "utf-8-sig") as csvfile:
        headers = next(csv.reader(csvfile))
        all_rows = list(csv.reader(csvfile))
        for a in all_rows:
            a_fund = {
                "name": a[0],
                "ref": f"https://www.google.com/search?q={a[0].replace(' ', '')}",
                # "ref": a[-1], # 可以動 (csv 多一個欄位)
            }
            fund_pool.append(a_fund)

    return fund_pool

def choose_fund(point, num_suggestion = 5, fold = 10):
    pool = fund_pool()

    level = int(point / 10) if point < 100 else (fold - 1)

    unit = int(len(pool) / fold)
    low_bound = unit * level
    high_bound = unit * (level + 1) if unit * (level + 1) != len(pool) else (len(pool) - 1)
    candidates = pool[low_bound: high_bound]

    suggest_list = random.sample(candidates, num_suggestion)

    return suggest_list
